# prpl components

## Introduction

This repo is used to download all code from https://gitlab.com/prpl-foundation/components

## Prerequisites

- Repo tool

```bash
curl https://storage.googleapis.com/git-repo-downloads/repo > ~/repo
chmod a+x ~/repo
sudo chown 0:0 ~/repo
sudo mv ~/repo /usr/bin/
```

## Fetching all libs and tools

```bash
mkdir ~/prpl_component
cd ~/prpl_component

repo init -u https://gitlab.com/willendzw/prpl-components.git
repo sync
repo forall -c "git checkout main"
```
